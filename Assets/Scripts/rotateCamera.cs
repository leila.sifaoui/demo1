using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotateCamera : MonoBehaviour
{
    public Camera Camera;

    private void Start()
    {
        Camera.transform.Rotate(-30, 0, 0);
    }

    // Update is called once per frame
    void Update()
    {
        transform.RotateAround(Vector3.zero, Vector3.up, 2 * Time.deltaTime);
    }
}
