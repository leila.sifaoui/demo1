using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class LoadingScreenManager : MonoBehaviour
{
    [SerializeField]
    private GameObject LoadingScreen;
    [SerializeField]
    private Slider loadingSlider;
    [SerializeField]
    private TMP_Text _loading;


    public void LoadScene(int sceneId)
    {
        loadingSlider.value = 0;
        LoadingScreen.SetActive(true);
        StartCoroutine(LoadSceneAsync(sceneId));
    }

    IEnumerator LoadSceneAsync(int sceneId)
    {
        
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneId);
        operation.allowSceneActivation = false;
        float progressValue = 0;

        while (!operation.isDone) {
          progressValue = Mathf.MoveTowards(progressValue, operation.progress, Time.deltaTime / 2);
          loadingSlider.value = progressValue;
          _loading.text = "Loading .. " + (progressValue * 100).ToString("00") + " %";

            if (progressValue >= 0.9f) {
                loadingSlider.value = 1;
                operation.allowSceneActivation = true;
            }

            

            yield return null;
        }
    }
}
