using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DataLoader : MonoBehaviour
{
    [SerializeField]
    private GameObject LoadingScreen;





    // Start is called before the first frame update
    void Start()
    {

        PlayFabLoad.Instance.isLoadFinished = false;
        //Debug.Log("Name found!");
        LoadingScreen.SetActive(true);
        StartCoroutine(Loader());

        //FIND THE GAME OBJECTS IN THE SCENE!
        

    }

    IEnumerator Loader()
    {

        //Debug.Log("Waiting for the data to be fetched...");
        
        yield return new WaitUntil(() => PlayFabLoad.Instance.isLoadFinished == true);
        PlayFabLoad.Instance.UpdateDataInScene();
        LoadingScreen.SetActive(false);
    }
}
