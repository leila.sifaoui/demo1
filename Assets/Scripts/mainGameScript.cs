using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class mainGameScript : MonoBehaviour
{
    [SerializeField] private GameObject template, template2, contentArea, StartPanel, GamePanel, FinishPanel, LeaderBoardPanel, TutoPanel;
    [SerializeField] private TMP_Text timerTxt, correctAnswerTxt, scoreTxt, accuracyTxt, totaltimeTxt, avgtimeTxt, gameTypeTxt;
    private float timer;
    private string[] shapes = {"Circle","Square", "Triangle"};
    private List<KeyValuePair<string, Color>> colors = new List<KeyValuePair<string, Color>>();
    public int questionCount = 0;
    private int correctCount;
    private float v;
    //private int correctV, wrongV = 1;
    public string correctAnswer, wrongAnswer;
    private bool isGameStarted, isGameShape = false;
    // Start is called before the first frame update
    void Start()
    {
        
        colors.Add(new KeyValuePair<string, Color>("red", Color.red));
        colors.Add(new KeyValuePair<string, Color>("blue", Color.blue));
        colors.Add(new KeyValuePair<string, Color>("yellow", Color.yellow));
        colors.Add(new KeyValuePair<string, Color>("white", Color.white));
        colors.Add(new KeyValuePair<string, Color>("green", Color.green));
        colors.Add(new KeyValuePair<string, Color>("magenta", Color.magenta));

    }

    public void StartGameShape()
    {
        

        foreach (Transform child in contentArea.transform)
        {
            Destroy(child.gameObject);
        }

        questionCount = 0; correctCount = 0;
        GamePanel.SetActive(true);
        StartPanel.SetActive(false);
        isGameStarted = true;
        timerTxt.text = timer.ToString();
        //INSTANTIATE THE QUESTION
        v = 0.0f;
        timer = 6.0f;
        gameTypeTxt.text = "SHAPE";
        isGameShape = true;
        InstantiateQuestion();
    }

    public void StartGameWord()
    {

        foreach (Transform child in contentArea.transform)
        {
            Destroy(child.gameObject);
        }

        Destroy(GameObject.FindGameObjectWithTag("Template"));
        questionCount = 0; correctCount = 0;
        GamePanel.SetActive(true);
        StartPanel.SetActive(false);
        isGameStarted = true;
        timerTxt.text = timer.ToString();
        //INSTANTIATE THE QUESTION
        v = 0.0f;
        timer = 6.0f;
        gameTypeTxt.text = "WORD";
        isGameShape = false;
        InstantiateQuestion();
    }


    //public void ShowLeaderBoard()
    //{
    //    PlayFabLoad.Instance.GetLeaderBoardShape();
    //    PlayFabLoad.Instance.GetLeaderBoardWord();
    //    StartPanel.SetActive(false);
    //    FinishPanel.SetActive(false);
    //    LeaderBoardPanel.SetActive(true);

    //}

    public void GoBack()
    {
        TutoPanel.SetActive(false);
        LeaderBoardPanel.SetActive(false);
        StartPanel.SetActive(true);
    }

    public void GoToTuto()
    {
        StartPanel.SetActive(false);
        TutoPanel.SetActive(true);
    }



    private int RandomProba()
    {
        int digit;

        digit = UnityEngine.Random.Range(0, 101);
        if (digit <= 55) {
           
            return 0;

        }else if ((digit >= 67 && digit <= 85))
        {
            
            return 1;
        }
        else return 2;
    }

    private int RandomColorKey()
    {
        if (RandomProba() == 0)
        {

            return 0;
        }
        else if (RandomProba() == 1)
        {

            return UnityEngine.Random.Range(1, 3);
        }
        else
        {

            return UnityEngine.Random.Range(3, colors.Count);
        }
    }

    private void InstantiateQuestion()
    {
        questionCount++;

        if (isGameShape)
        {
            

            GameObject go = Instantiate(template, template.transform.position, Quaternion.identity);
            go.transform.parent = contentArea.transform;
            go.transform.localScale = new Vector3(1, 1, 1);
            //string correctShape = shapes[UnityEngine.Random.Range(0, shapes.Length)];
            string correctShape = shapes[RandomProba()];
            
            int correctKey = RandomColorKey();

            correctAnswer = colors[correctKey].Key + " " + correctShape;
            //Debug.Log(correctAnswer);
            go.transform.GetChild(0).GetChild(0).GetComponent<UnityEngine.UI.Image>().sprite = Resources.Load<Sprite>("Question/" + correctShape);
            go.transform.GetChild(0).GetChild(0).GetComponent<UnityEngine.UI.Image>().color = colors[correctKey].Value;

            string wrongShape = shapes[RandomProba()];
           
            int wrongKey = RandomColorKey();

            wrongAnswer = colors[wrongKey].Key + " " + wrongShape;

            go.transform.GetChild(1).GetChild(0).GetComponent<TMP_Text>().text = wrongAnswer;
            go.transform.GetChild(1).GetChild(0).GetComponent<TMP_Text>().color = colors[RandomColorKey()].Value;
        }
        else
        {
            GameObject go = Instantiate(template2, template2.transform.position, Quaternion.identity);
            go.transform.parent = contentArea.transform;
            go.transform.localScale = new Vector3(1, 1, 1);
            int correctKey = RandomColorKey();

            correctAnswer = colors[correctKey].Key;
            
            go.transform.GetChild(0).GetChild(0).GetComponent<TMP_Text>().text = colors[RandomColorKey()].Key;
            go.transform.GetChild(0).GetChild(0).GetComponent<TMP_Text>().color = colors[correctKey].Value;

            int wrongKey = RandomColorKey();

            wrongAnswer = colors[wrongKey].Key;

            go.transform.GetChild(1).GetChild(0).GetComponent<TMP_Text>().text = wrongAnswer;
            go.transform.GetChild(1).GetChild(0).GetComponent<TMP_Text>().color = colors[RandomColorKey()].Value;
        }

        if (questionCount >= 13)
        {
            //RESULTS SCREEN

            isGameStarted = false;
            GamePanel.SetActive(false);
            FinishPanel.SetActive(true);
            
            correctAnswerTxt.text = correctCount + "/" + (questionCount - 1);
            int vitesse = (int)(v % 60);
            totaltimeTxt.text = vitesse.ToString() + " Seconds";

            float score = (float)(50 * correctCount)/vitesse;
            scoreTxt.text = score.ToString("00");

            if (correctCount == questionCount-1)
            {
                if (isGameShape) { PlayFabLoad.Instance.ShapeSendLeaderBoard((int)score); } else { PlayFabLoad.Instance.WordSendLeaderBoard((int)score); }
            }

            float accuracy = (float)correctCount / (questionCount - 1);
            accuracyTxt.text = (accuracy * 100).ToString("00.00") + "%";

            

            avgtimeTxt.text = ((float)vitesse / (questionCount - 1)).ToString("0.00") + " seconds";

            //KPI par PDF
            
        }
    }



    // Update is called once per frame
    void Update()
    { if(isGameStarted)
        TimeCountdown();
    }

    private void TimeCountdown()
    {
        
        v += Time.deltaTime;
        timer -= Time.deltaTime;
        // turn seconds in float to int
        int seconds = (int)(timer % 60);
        timerTxt.text = seconds.ToString();

        if (seconds == 0)
        {
            Destroy(GameObject.FindGameObjectWithTag("Template"));    
            InstantiateQuestion();
            timer = 6.0f;
            
        } 
                if (((correctAnswer == wrongAnswer) && (Input.GetKeyDown(KeyCode.RightArrow))) || ((correctAnswer != wrongAnswer) && (Input.GetKeyDown(KeyCode.LeftArrow))))
                {
            Answer(true);

                } else

            if (((correctAnswer != wrongAnswer) && (Input.GetKeyDown(KeyCode.RightArrow))) || ((correctAnswer == wrongAnswer) && (Input.GetKeyDown(KeyCode.LeftArrow))))
            {

            Answer(false);
        }
     }

    public void onClickRight()
    {
        if (correctAnswer == wrongAnswer)
        {
            Answer(true);
        }
        else
        {
            Answer(false);
        }
    }


    public void onClickLeft()
    {
        if (correctAnswer != wrongAnswer)
        {
            Answer(true);
        }else
        {
            Answer(false) ;
        }
    }

    private void Answer(bool answer)
    {
        if (answer)
        {
            correctCount++;
            //correctV = correctV * (int)timer;
            Destroy(GameObject.FindGameObjectWithTag("Template"));
            InstantiateQuestion();
            timer = 6.0f;
            
        } else
        {
            //correctV = correctV * (int)timer;
            Destroy(GameObject.FindGameObjectWithTag("Template"));
            InstantiateQuestion();
            timer = 6.0f;
            
        }
    }
}
