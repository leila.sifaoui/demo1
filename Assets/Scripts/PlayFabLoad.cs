using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using PlayFab;
using PlayFab.ClientModels;
using System;

public class PlayFabLoad : MonoBehaviour
{
    
    public static PlayFabLoad Instance;

    [SerializeField] GameObject leaderBoardItem;

    [SerializeField] LoadingScreenManager load;

    public bool isLoadFinished;

    private TMP_Text Gems, Name;

    public int gems;

    public string username;


    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }

       
    }
    // Start is called before the first frame update
    void Start()
    {
        
        GetPlayerData();
        isLoadFinished = false;



    }

    private void GetPlayerData()
    {
        PlayFabClientAPI.GetAccountInfo(new GetAccountInfoRequest {}, OnSuccess, OnError);
       


    }

    private void OnError(PlayFabError obj)
    {
        Debug.Log(obj.ErrorMessage);
        isLoadFinished = false;
    }

    public void OnSuccess(GetAccountInfoResult obj)
    {
        GetVirtualCurrency();
        Debug.Log(obj.AccountInfo.Username);
        username = obj.AccountInfo.Username;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GetVirtualCurrency()
    {
        PlayFabClientAPI.GetUserInventory(new GetUserInventoryRequest(), OnGetUserInventorySuccess, OnError);
    }

    private void OnGetUserInventorySuccess(GetUserInventoryResult result)
    {
         gems = result.VirtualCurrency["GM"];
        

        UpdateDataInScene();
        isLoadFinished = true;
    }

    public void Logout()
    {
        PlayFabClientAPI.ForgetAllCredentials();
        load.LoadScene(0);
    }

    public void GetItems()
    {
        
    }

    public void SpawnItems()
    {

    }



    public void UpdateDataInScene()
    {
        if (GameObject.Find("Name") != null)
        {
            Name = GameObject.Find("Name").GetComponent<TMP_Text>();
            Name.text = PlayFabLoad.Instance.username;
        }

        if (GameObject.Find("Gems") != null)
        {
            Gems = GameObject.Find("Gems").GetComponent<TMP_Text>();
            Gems.text = PlayFabLoad.Instance.gems.ToString();
        }

    }


    public void ShapeSendLeaderBoard(int score)
    {
        var request = new UpdatePlayerStatisticsRequest
        {
            Statistics = new List<StatisticUpdate>
            {
                new StatisticUpdate
                {
                    StatisticName = "ShapeLeaderboard",
                    Value = score
                }
            }
        };
        PlayFabClientAPI.UpdatePlayerStatistics(request, OnLeaderBoardUpdate, OnError);
    }


    public void WordSendLeaderBoard(int score)
    {
        var request = new UpdatePlayerStatisticsRequest
        {
            Statistics = new List<StatisticUpdate>
            {
                new StatisticUpdate
                {
                    StatisticName = "WordLeaderboard",
                    Value = score
                }
            }
        };
        PlayFabClientAPI.UpdatePlayerStatistics(request, OnLeaderBoardUpdate, OnError);
    }

    private void OnLeaderBoardUpdate(UpdatePlayerStatisticsResult obj)
    {
        Debug.Log("success");
    }

    public void GetLeaderBoardShape()
    {
        var request = new GetLeaderboardRequest
        {
            StatisticName = "ShapeLeaderboard",
           
            MaxResultsCount = 10
        };
        PlayFabClientAPI.GetLeaderboard(request, OnLeaderBoardGetShape, OnError);
    }

    public void GetLeaderBoardWord()
    {
        var request = new GetLeaderboardRequest
        {
            StatisticName = "WordLeaderboard",

            MaxResultsCount = 10
        };
        PlayFabClientAPI.GetLeaderboard(request, OnLeaderBoardGetWord, OnError);
    }

    private void OnLeaderBoardGetShape(GetLeaderboardResult result)
    {


        GameObject go = GameObject.FindGameObjectWithTag("shape");

        foreach(Transform child in go.transform)
        {
            Destroy(child.gameObject);
        }

        foreach (var item in result.Leaderboard)
        {
            GameObject field = Instantiate(leaderBoardItem, leaderBoardItem.transform.position, Quaternion.identity);
            field.transform.parent = go.transform;
            field.transform.localScale = new Vector3(1, 1, 1);
            field.transform.GetChild(0).GetComponent<TMP_Text>().text = (item.Position + 1).ToString();
            field.transform.GetChild(1).GetComponent<TMP_Text>().text = item.DisplayName;
            field.transform.GetChild(2).GetComponent<TMP_Text>().text = item.StatValue.ToString();

            //Debug.Log(item.Position + " " + item.DisplayName + " " + item.StatValue);
        }
    }

    private void OnLeaderBoardGetWord(GetLeaderboardResult result)
    {
        GameObject go = GameObject.FindGameObjectWithTag("word");

        foreach (Transform child in go.transform)
        {
            Destroy(child.gameObject);
        }

        foreach (var item in result.Leaderboard)
        {
            GameObject field = Instantiate(leaderBoardItem, leaderBoardItem.transform.position, Quaternion.identity);
            field.transform.parent = go.transform;
            field.transform.localScale = new Vector3(1, 1, 1);
            field.transform.GetChild(0).GetComponent<TMP_Text>().text = (item.Position + 1).ToString();
            field.transform.GetChild(1).GetComponent<TMP_Text>().text = item.DisplayName;
            field.transform.GetChild(2).GetComponent<TMP_Text>().text = item.StatValue.ToString();

            //Debug.Log(item.Position + " " + item.DisplayName + " " + item.StatValue);
        }
    }
}
