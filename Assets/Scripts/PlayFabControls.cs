using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;
using PlayFab;
using PlayFab.ClientModels;

public class PlayFabControls : MonoBehaviour
{
    EventSystem system;
    public Selectable firstInput;
    public Button submitButton;
    [SerializeField]
    private TMP_Text _message;
    [SerializeField] private TMP_InputField _usernameLogin, _passwordLogin, _usernameRegister, _passwordRegister;
    [SerializeField]
    private GameObject SignUpPanel, SignInPanel;
    string EncryptedPassword;

    [SerializeField] LoadingScreenManager load;
    // Start is called before the first frame update
    void Start()
    {
        
        system = EventSystem.current;
        firstInput.Select();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            Selectable next = system.currentSelectedGameObject.GetComponent<Selectable>().FindSelectableOnDown();   
            if (next != null) {
                next.Select();
            }
        } else if(Input.GetKeyDown(KeyCode.Return))
        {
            if ((_usernameLogin.text.Length != 0) && (_passwordLogin.text.Length != 0))
            {
                submitButton.onClick.Invoke();
                
                _message.text = "Logged successfully";
            }
            else
            {
                _message.text = "Please insert username and password";
                
            }
            
        }
    }

    public void ToSignInPanel()
    {
        SignInPanel.gameObject.SetActive(true);
        SignUpPanel.gameObject.SetActive(false);
        _message.text = "";


    }

    public void ToSignUpPanel()
    {
        SignUpPanel.gameObject.SetActive(true);
        SignInPanel.gameObject.SetActive(false);
        _message.text = "";


    }

    string Encrypt (string pass)
    {
        System.Security.Cryptography.MD5CryptoServiceProvider x = new System.Security.Cryptography.MD5CryptoServiceProvider();
        byte[] bs = System.Text.Encoding.UTF8.GetBytes(pass);
        bs = x.ComputeHash(bs);
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        foreach(byte b in bs)
        {
            sb.Append(b.ToString("x2").ToLower());
        }
        return sb.ToString();
    }

    public void SingUp()
    {
        if (_usernameRegister.text.Equals(""))
        {
            _message.text = "Username Cannot be empty";
            return;
        }

        if (_passwordRegister.text.Equals(""))
        {
            _message.text = "Password Cannot be empty";
            return;
        }

        if (_passwordRegister.text.Length < 6)
        {
            _message.text = "Password too short";
            return;
        }

        
        var request = new RegisterPlayFabUserRequest
        {
            
            Username = _usernameRegister.text,
            DisplayName = _usernameRegister.text,
            Password = Encrypt(_passwordRegister.text),
            RequireBothUsernameAndEmail = false
        };
        PlayFabClientAPI.RegisterPlayFabUser(request, OnRegisterSuccess, OnError);
    }

    void OnRegisterSuccess(RegisterPlayFabUserResult obj)
    {
        _message.text = "Registered and Logged in successfully";
        StartGame();
    }

    void OnError(PlayFabError obj)
    {
        _message.text = obj.ErrorMessage;
    }

    public void Login()
    {
        if (_usernameLogin.text.Equals(""))
        {
            _message.text = "Username Cannot be empty";
            return;
        }

        if (_passwordLogin.text.Equals(""))
        {
            _message.text = "Password Cannot be empty";
            return;
        }
        var request = new LoginWithPlayFabRequest { Username = _usernameLogin.text,
            Password = Encrypt(_passwordLogin.text) };
        PlayFabClientAPI.LoginWithPlayFab(request, LoginSuccess, LoginError);
    }

    private void LoginError(PlayFabError obj)
    {
        _message.text = obj.ErrorMessage;
    }

    private void LoginSuccess(LoginResult obj)
    {
        _message.text = "Logged in successfully";
        StartGame();
    }

    private void StartGame()
    {
        load.LoadScene(1);
    }
}

