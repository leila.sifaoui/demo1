﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void AnimationScript::Start()
extern void AnimationScript_Start_mAC1F54F92A07A5FB5D1138452446EA95F958B1B0 (void);
// 0x00000002 System.Void AnimationScript::Update()
extern void AnimationScript_Update_mB9A18E1CE8DD2F09B394721AC0B5188755EA7C9A (void);
// 0x00000003 System.Void AnimationScript::.ctor()
extern void AnimationScript__ctor_m3ADFA1175E3125D6C25902D78F6171CE65F841B3 (void);
// 0x00000004 System.Void EnemyAi::Start()
extern void EnemyAi_Start_m4C296C5B87C73D170D4F6A2A30D461DBDA82B809 (void);
// 0x00000005 System.Void EnemyAi::WalkToNextDestination()
extern void EnemyAi_WalkToNextDestination_m3D2146D1DF3A3C29F7C7DAB11EDD68A10978471E (void);
// 0x00000006 System.Void EnemyAi::CancelGoNextDestination()
extern void EnemyAi_CancelGoNextDestination_m3365BF19C3EEC3AF840B5BE4FAB6A2C79FF228B5 (void);
// 0x00000007 System.Void EnemyAi::SetFace(UnityEngine.Texture)
extern void EnemyAi_SetFace_m5EBD5E78FB4EDF3ED976460C42D4F1B7AD7F0F56 (void);
// 0x00000008 System.Void EnemyAi::Update()
extern void EnemyAi_Update_m4E092270689359F7804253D46F1343B48CCB3B0E (void);
// 0x00000009 System.Void EnemyAi::StopAgent()
extern void EnemyAi_StopAgent_m3C34DEC9A4404BD7A44D224F4E3092EEDAB10005 (void);
// 0x0000000A System.Void EnemyAi::AlertObservers(System.String)
extern void EnemyAi_AlertObservers_mB39B7D39FE6F7E7185DF96FD4D8E35C1DCA5F105 (void);
// 0x0000000B System.Void EnemyAi::OnAnimatorMove()
extern void EnemyAi_OnAnimatorMove_m4EB1A4493ECF5CFF898715D55C1BD1DD3A0B190F (void);
// 0x0000000C System.Void EnemyAi::.ctor()
extern void EnemyAi__ctor_mAFB0E106A58424FF2FC79C90C38F6F0C9D8023B6 (void);
// 0x0000000D System.Void Face::.ctor()
extern void Face__ctor_m0125CAE047B24F4895C02441B83D05D1C08B433D (void);
// 0x0000000E System.Void GameManager::Start()
extern void GameManager_Start_m87A71D65F3171A58DBDDBFB03832ADA65643D0E2 (void);
// 0x0000000F System.Void GameManager::Idle()
extern void GameManager_Idle_m1B814D24018A953231A95CE921D527F5D72568A8 (void);
// 0x00000010 System.Void GameManager::ChangeStateTo(SlimeAnimationState)
extern void GameManager_ChangeStateTo_m0EB6EEFACB0A16F6049687D95B210A230B7A1162 (void);
// 0x00000011 System.Void GameManager::LookAtCamera()
extern void GameManager_LookAtCamera_m69EA395CCB0C68DCFAC490A049336DFBB8485B2F (void);
// 0x00000012 System.Void GameManager::.ctor()
extern void GameManager__ctor_mF453CED520617BFB65C52405A964E06CF17DB368 (void);
// 0x00000013 System.Void GameManager::<Start>b__9_0()
extern void GameManager_U3CStartU3Eb__9_0_m29368EA0AB7549755F9B1CDC2414EFE2658A392D (void);
// 0x00000014 System.Void GameManager::<Start>b__9_1()
extern void GameManager_U3CStartU3Eb__9_1_mF245E1278F28FA57342A322B04BBDAC0827CA898 (void);
// 0x00000015 System.Void GameManager::<Start>b__9_2()
extern void GameManager_U3CStartU3Eb__9_2_m0886715BEAE34D92EFCCF57FBCC819F6A4DE5E3E (void);
// 0x00000016 System.Void GameManager::<Start>b__9_3()
extern void GameManager_U3CStartU3Eb__9_3_mDDDEA84547A9E737B147D0FB43E1DF821E76C4BC (void);
// 0x00000017 System.Void GameManager::<Start>b__9_4()
extern void GameManager_U3CStartU3Eb__9_4_m1FA0506EFC2915A80D6C34E447716015910FA92A (void);
// 0x00000018 System.Void GameManager::<Start>b__9_5()
extern void GameManager_U3CStartU3Eb__9_5_m3FA02DC4D3D232660056CCBDD001B7F5852DF4E6 (void);
// 0x00000019 System.Void GameManager::<Start>b__9_6()
extern void GameManager_U3CStartU3Eb__9_6_m6592677B3AEC7CE85504C0F597A60CF7AB43024B (void);
// 0x0000001A System.Void CameraMove::Start()
extern void CameraMove_Start_m9BC4AAF6D2DC912359A2DB5C7689DB27DBF4BA51 (void);
// 0x0000001B System.Void CameraMove::Update()
extern void CameraMove_Update_mE9E960AB865A8E65A6E1E57ABA98827B43DBE9BC (void);
// 0x0000001C System.Void CameraMove::Rotate()
extern void CameraMove_Rotate_m93DA0C1A259946EB0083EAA54C186913E95A6315 (void);
// 0x0000001D System.Void CameraMove::.ctor()
extern void CameraMove__ctor_m3BBC2F541B40FB1634F0EA0B844BA27E5DDD9908 (void);
// 0x0000001E System.Void CollectingHearts::Start()
extern void CollectingHearts_Start_m5F2B6A61085215175C944DAD4EBC37E8850CE31F (void);
// 0x0000001F System.Void CollectingHearts::OnTriggerEnter(UnityEngine.Collider)
extern void CollectingHearts_OnTriggerEnter_mA1E6F93DE7ACFEBD7433F4A35BD027E480F20E11 (void);
// 0x00000020 System.Void CollectingHearts::PauseGame()
extern void CollectingHearts_PauseGame_mAD991EDE285D2A361E2E60425814A14078852CBA (void);
// 0x00000021 System.Void CollectingHearts::ResumeGame()
extern void CollectingHearts_ResumeGame_m40C2B3C8EE43E4F31DB42E651581C2FAF775E3A0 (void);
// 0x00000022 System.Void CollectingHearts::.ctor()
extern void CollectingHearts__ctor_mB8075967156DB1B72C2E0F1940FB5E715F4B5C21 (void);
// 0x00000023 System.Void DataLoader::Start()
extern void DataLoader_Start_m709E7020FCC56E283A6673EA1DEDD2242C8EC3F3 (void);
// 0x00000024 System.Collections.IEnumerator DataLoader::Loader()
extern void DataLoader_Loader_mF42BAE84DD79A156760C9C828504417DE58D74E6 (void);
// 0x00000025 System.Void DataLoader::.ctor()
extern void DataLoader__ctor_m33F58839236A971EB235C4D9B49E4876A76FB9A6 (void);
// 0x00000026 System.Void DataLoader/<>c::.cctor()
extern void U3CU3Ec__cctor_m417DC2AF817231E3B67113909A95E3648FD91127 (void);
// 0x00000027 System.Void DataLoader/<>c::.ctor()
extern void U3CU3Ec__ctor_mA07A16D73A6BFBBAA099837DDF24F66234E4544F (void);
// 0x00000028 System.Boolean DataLoader/<>c::<Loader>b__2_0()
extern void U3CU3Ec_U3CLoaderU3Eb__2_0_m5E88B435BA060F648969A8C06B51509BDBE9B7EE (void);
// 0x00000029 System.Void DataLoader/<Loader>d__2::.ctor(System.Int32)
extern void U3CLoaderU3Ed__2__ctor_m1C22865F6551EC4486AB0132E349B86BF442C5BC (void);
// 0x0000002A System.Void DataLoader/<Loader>d__2::System.IDisposable.Dispose()
extern void U3CLoaderU3Ed__2_System_IDisposable_Dispose_mD5A993D64000661634B4E628D38A6AABA3BFE9AD (void);
// 0x0000002B System.Boolean DataLoader/<Loader>d__2::MoveNext()
extern void U3CLoaderU3Ed__2_MoveNext_m04292C42B43E416E6F557F6E3834C21C845DB37C (void);
// 0x0000002C System.Object DataLoader/<Loader>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoaderU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFE49CEED3A1A69BB5CB72250C410FF2605E240FE (void);
// 0x0000002D System.Void DataLoader/<Loader>d__2::System.Collections.IEnumerator.Reset()
extern void U3CLoaderU3Ed__2_System_Collections_IEnumerator_Reset_m809646CB0C0D8F92E17262F609A069874148F3F8 (void);
// 0x0000002E System.Object DataLoader/<Loader>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CLoaderU3Ed__2_System_Collections_IEnumerator_get_Current_m19FBF2A640655D46061C4B2072EF252BFED6098B (void);
// 0x0000002F System.Void funSceneScript::Start()
extern void funSceneScript_Start_m3795C7F1516764F9E269EFF8558DCF2906D8A6CA (void);
// 0x00000030 System.Void funSceneScript::StartGameShape()
extern void funSceneScript_StartGameShape_m20DCD398EBCB4D48F93EF50E0ACECD7B1AB10DC1 (void);
// 0x00000031 System.Void funSceneScript::StartGameWord()
extern void funSceneScript_StartGameWord_m5D05708C49F5AAE608C2B2782F688767C58D0157 (void);
// 0x00000032 System.Void funSceneScript::ShowLeaderBoard()
extern void funSceneScript_ShowLeaderBoard_m229D4C91E6780AF4B9B0AA45075C6E382AD2A654 (void);
// 0x00000033 System.Void funSceneScript::GoBack()
extern void funSceneScript_GoBack_m1C1211F929D16334D4871458D7A8B60429888B2E (void);
// 0x00000034 System.Void funSceneScript::GoToTuto()
extern void funSceneScript_GoToTuto_mD0F1120FAC113933D90838CDE71BFF978B727089 (void);
// 0x00000035 System.Int32 funSceneScript::RandomProba()
extern void funSceneScript_RandomProba_m17E5EEB06E7F241D1E5778493F82567DA8DA378B (void);
// 0x00000036 System.Int32 funSceneScript::RandomColorKey()
extern void funSceneScript_RandomColorKey_m3218DDF5720AC64A141BC35ACDBDA7AAE9C612A2 (void);
// 0x00000037 System.Void funSceneScript::InstantiateQuestion()
extern void funSceneScript_InstantiateQuestion_mDCECC14624D28F88B5DBF9E80E33FF8C44FCB438 (void);
// 0x00000038 System.Void funSceneScript::Update()
extern void funSceneScript_Update_mB7C51ACBB4595DB8DFCC4A6188C78A28F519F64A (void);
// 0x00000039 System.Void funSceneScript::TimeCountdown()
extern void funSceneScript_TimeCountdown_mD7B5B680FB355FCE1544E6ACC6C2F8C3600E72BB (void);
// 0x0000003A System.Void funSceneScript::onClickRight()
extern void funSceneScript_onClickRight_m44CC79043D306386F7F57C89404535E4B6A36D73 (void);
// 0x0000003B System.Void funSceneScript::onClickLeft()
extern void funSceneScript_onClickLeft_mB5C466B3870D5DA70BA34B221AC573E05920EAEC (void);
// 0x0000003C System.Void funSceneScript::Answer(System.Boolean)
extern void funSceneScript_Answer_m5E140C2EAFE842C51A32C683D96554396CF8B4A6 (void);
// 0x0000003D System.Void funSceneScript::.ctor()
extern void funSceneScript__ctor_mB373BAF18646E039EFAD19AF1E761F1071536949 (void);
// 0x0000003E System.Void HomeScript::Start()
extern void HomeScript_Start_m46637A2DF00461BAB2FA53661788E7326936B191 (void);
// 0x0000003F System.Void HomeScript::GetVirtualCurrency()
extern void HomeScript_GetVirtualCurrency_mDBD6E357AE663F715FF9C5A1CFBACBDCA5F9A927 (void);
// 0x00000040 System.Void HomeScript::Update()
extern void HomeScript_Update_m310EB584A5A15E10AE2334F2F5271334A2AFC949 (void);
// 0x00000041 System.Void HomeScript::.ctor()
extern void HomeScript__ctor_mCDBE87645B9E8E89256F05CD133AADE1B8CFD2AB (void);
// 0x00000042 System.Void HomeScript::<GetVirtualCurrency>b__3_0(PlayFab.ClientModels.GetUserInventoryResult)
extern void HomeScript_U3CGetVirtualCurrencyU3Eb__3_0_m21DFCB754583341A95124365E6A0CB258930267C (void);
// 0x00000043 System.Void HomeScript/<>c::.cctor()
extern void U3CU3Ec__cctor_m6A90C203D144C391CECDF322C2E3B6271D04FE4B (void);
// 0x00000044 System.Void HomeScript/<>c::.ctor()
extern void U3CU3Ec__ctor_m44983C2F8775B7812CC8F69FF220BA31D0EBDB71 (void);
// 0x00000045 System.Void HomeScript/<>c::<GetVirtualCurrency>b__3_1(PlayFab.PlayFabError)
extern void U3CU3Ec_U3CGetVirtualCurrencyU3Eb__3_1_mDC0EE5D5E6B3115D56DA5C08E2BE811E8B300139 (void);
// 0x00000046 System.Void InventoryScript::Start()
extern void InventoryScript_Start_m4318FC1D1E6FB13FF29E29DDDC0A3053FA12A626 (void);
// 0x00000047 System.Void InventoryScript::SpawnCardsInInventory(UnityEngine.GameObject)
extern void InventoryScript_SpawnCardsInInventory_m1A6326ACD9905728220A7BBCCF8C24E555DEE5C5 (void);
// 0x00000048 System.Void InventoryScript::.ctor()
extern void InventoryScript__ctor_m7AF92B24D0AC8339DA8BFACD471A96FCB5A3D26D (void);
// 0x00000049 System.Void InventoryScript/<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_mB9A1E96A96201727DE4A85CD818A84572F49327C (void);
// 0x0000004A System.Void InventoryScript/<>c__DisplayClass2_0::<SpawnCardsInInventory>b__0(PlayFab.ClientModels.GetUserInventoryResult)
extern void U3CU3Ec__DisplayClass2_0_U3CSpawnCardsInInventoryU3Eb__0_m386097060903085E4682C2AD323D6F93191096A2 (void);
// 0x0000004B System.Void InventoryScript/<>c::.cctor()
extern void U3CU3Ec__cctor_m9670502EA23C39DC5F131322B7CD456A7BFC11DD (void);
// 0x0000004C System.Void InventoryScript/<>c::.ctor()
extern void U3CU3Ec__ctor_mDB5643258239D638957734AA23AA6C86CA95C84C (void);
// 0x0000004D System.Void InventoryScript/<>c::<SpawnCardsInInventory>b__2_1(PlayFab.PlayFabError)
extern void U3CU3Ec_U3CSpawnCardsInInventoryU3Eb__2_1_mDCEC39AA086E1A52043D1823FFD24EEED66D0099 (void);
// 0x0000004E System.Void Item::Start()
extern void Item_Start_mAF304BC26FB5ED4C69F6534266EE4F082967E4D8 (void);
// 0x0000004F System.Void Item::BuyItem()
extern void Item_BuyItem_m2111DBEEF26EDD456E27CE22F648511F6BAF3FDE (void);
// 0x00000050 System.Void Item::onSuccessPurchase(PlayFab.ClientModels.PurchaseItemResult)
extern void Item_onSuccessPurchase_m37750C6CAF6B13C4CE5BE1A9C4C4121B65923040 (void);
// 0x00000051 System.Void Item::onSuccess(PlayFab.ClientModels.ModifyUserVirtualCurrencyResult)
extern void Item_onSuccess_mC5F610693487822D231D0CAB8FE40DB457D385CD (void);
// 0x00000052 System.Void Item::onError(PlayFab.PlayFabError)
extern void Item_onError_m2FB46161DEEA0AAFBD52A24B9CF4AAFED7DA33AA (void);
// 0x00000053 System.Void Item::.ctor()
extern void Item__ctor_m741D59B05082743C60D2F1149112B571E89CAFAF (void);
// 0x00000054 System.Void LoadingScreenManager::LoadScene(System.Int32)
extern void LoadingScreenManager_LoadScene_m823C4472BD68911292132F5309BCAF4DC56F16D3 (void);
// 0x00000055 System.Collections.IEnumerator LoadingScreenManager::LoadSceneAsync(System.Int32)
extern void LoadingScreenManager_LoadSceneAsync_m49B81CF3E4B7B9704DD289B10CED98416C01D12C (void);
// 0x00000056 System.Void LoadingScreenManager::.ctor()
extern void LoadingScreenManager__ctor_mE31332B5B0F7414BB3C95D11FDB397E471D75C64 (void);
// 0x00000057 System.Void LoadingScreenManager/<LoadSceneAsync>d__4::.ctor(System.Int32)
extern void U3CLoadSceneAsyncU3Ed__4__ctor_mF8C9188D135C46E469424012BE8399F6784EF3A5 (void);
// 0x00000058 System.Void LoadingScreenManager/<LoadSceneAsync>d__4::System.IDisposable.Dispose()
extern void U3CLoadSceneAsyncU3Ed__4_System_IDisposable_Dispose_m008D03B3E3B41870AF08FB9D28662D2379DC349F (void);
// 0x00000059 System.Boolean LoadingScreenManager/<LoadSceneAsync>d__4::MoveNext()
extern void U3CLoadSceneAsyncU3Ed__4_MoveNext_m1495E943131575F35340DD50041A25D98993F775 (void);
// 0x0000005A System.Object LoadingScreenManager/<LoadSceneAsync>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadSceneAsyncU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3C9C6775CDE3DFAA853173A02FD10F279B81E331 (void);
// 0x0000005B System.Void LoadingScreenManager/<LoadSceneAsync>d__4::System.Collections.IEnumerator.Reset()
extern void U3CLoadSceneAsyncU3Ed__4_System_Collections_IEnumerator_Reset_mCEFCC4FED8CDD4763CBEAC46D0EC5FB82FF41339 (void);
// 0x0000005C System.Object LoadingScreenManager/<LoadSceneAsync>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CLoadSceneAsyncU3Ed__4_System_Collections_IEnumerator_get_Current_m1AA19E6D7A57DC10DB64FCA8139A5A8054724C43 (void);
// 0x0000005D System.Void PlayerMouv::Start()
extern void PlayerMouv_Start_m1ECD55989605232DC8D390D5DC487F7E5CDE9D95 (void);
// 0x0000005E System.Void PlayerMouv::Update()
extern void PlayerMouv_Update_m19D4D156D951023F45EAF9F5FE259672CFCD7FB0 (void);
// 0x0000005F System.Void PlayerMouv::Move()
extern void PlayerMouv_Move_mF55E47F6604D1ED4FA9B184CCF34555771A2F2E3 (void);
// 0x00000060 System.Void PlayerMouv::Idle()
extern void PlayerMouv_Idle_mC96FE4B4FA7B2FCDF8BC948F9D6CF620A25AD277 (void);
// 0x00000061 System.Void PlayerMouv::Walk()
extern void PlayerMouv_Walk_mB1A1EEEDADD306F38B0ABD5BD9F5BFC3C03351B2 (void);
// 0x00000062 System.Void PlayerMouv::Run()
extern void PlayerMouv_Run_m9D92B6975AA4E5BB7FE224790C90CF4413A58CD3 (void);
// 0x00000063 System.Void PlayerMouv::Jump()
extern void PlayerMouv_Jump_m803E3A3FF4F6E53684DD90820BE0839F5FDF3D35 (void);
// 0x00000064 System.Void PlayerMouv::.ctor()
extern void PlayerMouv__ctor_m59281E1A3294DC3E6E6D49F9CBE53A3A4183DA8F (void);
// 0x00000065 System.Void PlayFabControls::Start()
extern void PlayFabControls_Start_m7A6CDC8E48983241857C3955BCA8159EE57A1FA4 (void);
// 0x00000066 System.Void PlayFabControls::Update()
extern void PlayFabControls_Update_m24B2D5174C360A4674DF638261F37910CF1BC04E (void);
// 0x00000067 System.Void PlayFabControls::ToSignInPanel()
extern void PlayFabControls_ToSignInPanel_mBACA29C524623D3E627A6E3B048C610FFE77DD2C (void);
// 0x00000068 System.Void PlayFabControls::ToSignUpPanel()
extern void PlayFabControls_ToSignUpPanel_m26579C369D73A44227F231D5E684C4F245D16D91 (void);
// 0x00000069 System.String PlayFabControls::Encrypt(System.String)
extern void PlayFabControls_Encrypt_mD046273CBC06052F6BF611740EEE901C702546C5 (void);
// 0x0000006A System.Void PlayFabControls::SingUp()
extern void PlayFabControls_SingUp_mAC4145AB251F22D908E6134DB0D921245FE8A70D (void);
// 0x0000006B System.Void PlayFabControls::OnRegisterSuccess(PlayFab.ClientModels.RegisterPlayFabUserResult)
extern void PlayFabControls_OnRegisterSuccess_mE7A83E89023F6344DDC8082BDFCE6B6487C8FC2D (void);
// 0x0000006C System.Void PlayFabControls::OnError(PlayFab.PlayFabError)
extern void PlayFabControls_OnError_m4A9059F6C1454FB10F0FB57DE44938EF995208EC (void);
// 0x0000006D System.Void PlayFabControls::Login()
extern void PlayFabControls_Login_mB1B4CC9AACBF9F642EA958116C088ED2BC7D27C4 (void);
// 0x0000006E System.Void PlayFabControls::LoginError(PlayFab.PlayFabError)
extern void PlayFabControls_LoginError_m2D4100CE9CC6754AC248FD8A7DA579A3CA3CA077 (void);
// 0x0000006F System.Void PlayFabControls::LoginSuccess(PlayFab.ClientModels.LoginResult)
extern void PlayFabControls_LoginSuccess_mE245340D080124FF3C992986DB6FF7F0559BDE4A (void);
// 0x00000070 System.Void PlayFabControls::StartGame()
extern void PlayFabControls_StartGame_m85D7B86D4B84E372C41EB4E4EDD491364014C220 (void);
// 0x00000071 System.Void PlayFabControls::.ctor()
extern void PlayFabControls__ctor_m9757752ECF18D8BFCB12C497FF6599182C8B1A00 (void);
// 0x00000072 System.Void PlayFabLoad::Awake()
extern void PlayFabLoad_Awake_m27AD8339F852574D6378BDC40EBB1EF88116AB57 (void);
// 0x00000073 System.Void PlayFabLoad::Start()
extern void PlayFabLoad_Start_mE9EF3C2569F4DF0D5E9A6338437C23A552DA881E (void);
// 0x00000074 System.Void PlayFabLoad::GetPlayerData()
extern void PlayFabLoad_GetPlayerData_m786D9F5B57DDDA10CA2B92F57666E2BE3C844A9B (void);
// 0x00000075 System.Void PlayFabLoad::OnError(PlayFab.PlayFabError)
extern void PlayFabLoad_OnError_m6CFB38002AC2F377FBB7CCFED842F559E122617A (void);
// 0x00000076 System.Void PlayFabLoad::OnSuccess(PlayFab.ClientModels.GetAccountInfoResult)
extern void PlayFabLoad_OnSuccess_m5792971F46A9659956283BDAA4587EC3321AFA58 (void);
// 0x00000077 System.Void PlayFabLoad::Update()
extern void PlayFabLoad_Update_m823904211C6EE00633D9E81322B34F2545A400DF (void);
// 0x00000078 System.Void PlayFabLoad::GetVirtualCurrency()
extern void PlayFabLoad_GetVirtualCurrency_m3691A467586730E966399FCAF96A8361EE25B78E (void);
// 0x00000079 System.Void PlayFabLoad::OnGetUserInventorySuccess(PlayFab.ClientModels.GetUserInventoryResult)
extern void PlayFabLoad_OnGetUserInventorySuccess_mD026BF7A4C3EC7B401C93BCE91DF470E59DD5313 (void);
// 0x0000007A System.Void PlayFabLoad::Logout()
extern void PlayFabLoad_Logout_m22F919A55F2FA9547061EAA655CE08B0E1AEAA35 (void);
// 0x0000007B System.Void PlayFabLoad::GetItems()
extern void PlayFabLoad_GetItems_m7BE33BC810B7EEC47C923169B4784C6E913448C8 (void);
// 0x0000007C System.Void PlayFabLoad::SpawnItems()
extern void PlayFabLoad_SpawnItems_mF52B7D2D9589EEF970716E774167CB4BE2B87C5D (void);
// 0x0000007D System.Void PlayFabLoad::UpdateDataInScene()
extern void PlayFabLoad_UpdateDataInScene_m62582E8EDDD8D003A49BB57FE88FDF97929192DA (void);
// 0x0000007E System.Void PlayFabLoad::ShapeSendLeaderBoard(System.Int32)
extern void PlayFabLoad_ShapeSendLeaderBoard_m20D75366E160EF2D3F05B5602B934CAD4E3814B7 (void);
// 0x0000007F System.Void PlayFabLoad::WordSendLeaderBoard(System.Int32)
extern void PlayFabLoad_WordSendLeaderBoard_m942C5C13D43B7E7E291230F841D7AA77BA51FC4D (void);
// 0x00000080 System.Void PlayFabLoad::OnLeaderBoardUpdate(PlayFab.ClientModels.UpdatePlayerStatisticsResult)
extern void PlayFabLoad_OnLeaderBoardUpdate_m08AC5EC0D85182933FB049E1903E90CE204EB743 (void);
// 0x00000081 System.Void PlayFabLoad::GetLeaderBoardShape()
extern void PlayFabLoad_GetLeaderBoardShape_m2281FFBD5A83FD39843A94FFDB9517D653E7CB2F (void);
// 0x00000082 System.Void PlayFabLoad::GetLeaderBoardWord()
extern void PlayFabLoad_GetLeaderBoardWord_m9A3BCC9522AB2C3A8A249F38C1A646A56546083F (void);
// 0x00000083 System.Void PlayFabLoad::OnLeaderBoardGetShape(PlayFab.ClientModels.GetLeaderboardResult)
extern void PlayFabLoad_OnLeaderBoardGetShape_m7CC311B539EB13AA8022DA3C063D35A81ABB34E0 (void);
// 0x00000084 System.Void PlayFabLoad::OnLeaderBoardGetWord(PlayFab.ClientModels.GetLeaderboardResult)
extern void PlayFabLoad_OnLeaderBoardGetWord_m144B524D91FA15AA4B6310919B4FA75FFE2BDE51 (void);
// 0x00000085 System.Void PlayFabLoad::.ctor()
extern void PlayFabLoad__ctor_m645447EB5F8FB3FF9E6629599C9919C22FDA5CD1 (void);
// 0x00000086 System.Void rotateCamera::Start()
extern void rotateCamera_Start_m93E06F1A9FEF74BAF2A47C14E7584828CE13760C (void);
// 0x00000087 System.Void rotateCamera::Update()
extern void rotateCamera_Update_m1A67811A414A7B9A39E66B747FB4512A8270B86B (void);
// 0x00000088 System.Void rotateCamera::.ctor()
extern void rotateCamera__ctor_m6996E5AFAB42DA2CAD20BBD5A0D89D9D0A989848 (void);
// 0x00000089 System.Void StoreScript::Start()
extern void StoreScript_Start_m5114A80C6D071937AFE843C7AB27F811FD77FF73 (void);
// 0x0000008A System.Void StoreScript::SpawnCardsInStore(UnityEngine.GameObject)
extern void StoreScript_SpawnCardsInStore_m46ABE21977A9C55C05C31212DA20DB44C951A8F1 (void);
// 0x0000008B System.Void StoreScript::.ctor()
extern void StoreScript__ctor_m83569B6502B9B8B35A6CFDB51ABC815E875A9E30 (void);
// 0x0000008C System.Void StoreScript/<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_m8673C710F9DFA85E5357E9C443779549A9C6A135 (void);
// 0x0000008D System.Void StoreScript/<>c__DisplayClass2_0::<SpawnCardsInStore>b__0(PlayFab.ClientModels.GetCatalogItemsResult)
extern void U3CU3Ec__DisplayClass2_0_U3CSpawnCardsInStoreU3Eb__0_m7C50F758398361106429A37EE363077E0CDEEC3C (void);
// 0x0000008E System.Void StoreScript/<>c::.cctor()
extern void U3CU3Ec__cctor_m21AFC931D1509D7EF8A21AE21F83F124B9EBF3FD (void);
// 0x0000008F System.Void StoreScript/<>c::.ctor()
extern void U3CU3Ec__ctor_m61B4D48A22F0EAF55FDF885CAA55B0A1BD315683 (void);
// 0x00000090 System.Void StoreScript/<>c::<SpawnCardsInStore>b__2_1(PlayFab.PlayFabError)
extern void U3CU3Ec_U3CSpawnCardsInStoreU3Eb__2_1_m552AE3DF7606B29A07A203C5B25B644EF1FE349A (void);
// 0x00000091 System.Void testSceneScript::Start()
extern void testSceneScript_Start_mC3A592D5A1873C81BC22C160745BC445EA6ACCAC (void);
// 0x00000092 System.Int32 testSceneScript::RandomProba()
extern void testSceneScript_RandomProba_m16A5807F01FE9475B5114E0A08B510E46DE63729 (void);
// 0x00000093 System.Int32 testSceneScript::RandomColorKey()
extern void testSceneScript_RandomColorKey_mE807223976F5FBCCF1FB7576078FED68290949FB (void);
// 0x00000094 System.Void testSceneScript::StartGameShape()
extern void testSceneScript_StartGameShape_m4B1EA5E1632F85EF5307F1ED3173E4F17E08B1B5 (void);
// 0x00000095 System.Void testSceneScript::StartGameWord()
extern void testSceneScript_StartGameWord_m8C7C9BD67F112D609717965843151A2EAF081079 (void);
// 0x00000096 System.Void testSceneScript::InstantiateQuestion()
extern void testSceneScript_InstantiateQuestion_m7013473D9A3AB0C4958490FC317F30B0F0CDC603 (void);
// 0x00000097 System.Void testSceneScript::Update()
extern void testSceneScript_Update_m7CC05B6DCAA0BA93FF095C5FBC3AFB2B3E1E1487 (void);
// 0x00000098 System.Void testSceneScript::TimeCountdown()
extern void testSceneScript_TimeCountdown_m2C9FA57BE7B6DE9C171405809D12E531EF8A92E9 (void);
// 0x00000099 System.Void testSceneScript::onClickRight()
extern void testSceneScript_onClickRight_m2BC016EF691C335B788990A06A666E3C345C8415 (void);
// 0x0000009A System.Void testSceneScript::onClickLeft()
extern void testSceneScript_onClickLeft_mF21360C0F118BE7BAC986A29C26879A0DDCEEDDE (void);
// 0x0000009B System.Void testSceneScript::Answer(System.Boolean)
extern void testSceneScript_Answer_m345AF5F3E370F02FA2078024BE625CB3390E336B (void);
// 0x0000009C System.Void testSceneScript::.ctor()
extern void testSceneScript__ctor_m687CD5473FECC273DCABB249022534CEBDE163D5 (void);
// 0x0000009D System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::Start()
extern void AbstractTargetFollower_Start_m900ACFC81D342429D83E274C92B69467E443763D (void);
// 0x0000009E System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::FixedUpdate()
extern void AbstractTargetFollower_FixedUpdate_mB21B34361FB6322B028B8087893942F3E9A11A22 (void);
// 0x0000009F System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::LateUpdate()
extern void AbstractTargetFollower_LateUpdate_m15329BC030A27BD5C92BCF0B9A2E7DC27C4ED202 (void);
// 0x000000A0 System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::ManualUpdate()
extern void AbstractTargetFollower_ManualUpdate_mC0EDA156049123462E7E0507191183A112209C58 (void);
// 0x000000A1 System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::FollowTarget(System.Single)
// 0x000000A2 System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::FindAndTargetPlayer()
extern void AbstractTargetFollower_FindAndTargetPlayer_mEBAED59C944A601F015CE6390C110B00EA882F2D (void);
// 0x000000A3 System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::SetTarget(UnityEngine.Transform)
extern void AbstractTargetFollower_SetTarget_m28BD1FECB5D3F99925BCA617888401258BD8A28C (void);
// 0x000000A4 UnityEngine.Transform UnityStandardAssets.Cameras.AbstractTargetFollower::get_Target()
extern void AbstractTargetFollower_get_Target_m0A358E204FE5247E0702B24B3D2047E89815A3D0 (void);
// 0x000000A5 System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::.ctor()
extern void AbstractTargetFollower__ctor_m7BA92F6CC37CDA403608738E4F6CB8EB43889A98 (void);
// 0x000000A6 System.Void UnityStandardAssets.Cameras.FreeLookCam::Awake()
extern void FreeLookCam_Awake_m6DF1C77903C277F5D7111A267F9B71B708A50867 (void);
// 0x000000A7 System.Void UnityStandardAssets.Cameras.FreeLookCam::Update()
extern void FreeLookCam_Update_m1158A615C803765D6375B456ADB1DDF93D8E7FB8 (void);
// 0x000000A8 System.Void UnityStandardAssets.Cameras.FreeLookCam::OnDisable()
extern void FreeLookCam_OnDisable_mCD044A476BAF96C34A06162121D3D21664AD438E (void);
// 0x000000A9 System.Void UnityStandardAssets.Cameras.FreeLookCam::FollowTarget(System.Single)
extern void FreeLookCam_FollowTarget_m807D7425DA3AC99BCFC995FC3F53B3139F50FE7E (void);
// 0x000000AA System.Void UnityStandardAssets.Cameras.FreeLookCam::HandleRotationMovement()
extern void FreeLookCam_HandleRotationMovement_m0E9A0819AA068EFA603FB0CC3318FBE71827A97F (void);
// 0x000000AB System.Void UnityStandardAssets.Cameras.FreeLookCam::.ctor()
extern void FreeLookCam__ctor_m0B5161AC16134D026A74E23EDE3011F252B9C28B (void);
// 0x000000AC System.Void UnityStandardAssets.Cameras.PivotBasedCameraRig::Awake()
extern void PivotBasedCameraRig_Awake_m3477C8EE2F83D51CCF81ADC1F8BEEDCB0A2F95B2 (void);
// 0x000000AD System.Void UnityStandardAssets.Cameras.PivotBasedCameraRig::.ctor()
extern void PivotBasedCameraRig__ctor_m176687FB94C7FF07D4ED8B38A0529F370F9D51BA (void);
static Il2CppMethodPointer s_methodPointers[173] = 
{
	AnimationScript_Start_mAC1F54F92A07A5FB5D1138452446EA95F958B1B0,
	AnimationScript_Update_mB9A18E1CE8DD2F09B394721AC0B5188755EA7C9A,
	AnimationScript__ctor_m3ADFA1175E3125D6C25902D78F6171CE65F841B3,
	EnemyAi_Start_m4C296C5B87C73D170D4F6A2A30D461DBDA82B809,
	EnemyAi_WalkToNextDestination_m3D2146D1DF3A3C29F7C7DAB11EDD68A10978471E,
	EnemyAi_CancelGoNextDestination_m3365BF19C3EEC3AF840B5BE4FAB6A2C79FF228B5,
	EnemyAi_SetFace_m5EBD5E78FB4EDF3ED976460C42D4F1B7AD7F0F56,
	EnemyAi_Update_m4E092270689359F7804253D46F1343B48CCB3B0E,
	EnemyAi_StopAgent_m3C34DEC9A4404BD7A44D224F4E3092EEDAB10005,
	EnemyAi_AlertObservers_mB39B7D39FE6F7E7185DF96FD4D8E35C1DCA5F105,
	EnemyAi_OnAnimatorMove_m4EB1A4493ECF5CFF898715D55C1BD1DD3A0B190F,
	EnemyAi__ctor_mAFB0E106A58424FF2FC79C90C38F6F0C9D8023B6,
	Face__ctor_m0125CAE047B24F4895C02441B83D05D1C08B433D,
	GameManager_Start_m87A71D65F3171A58DBDDBFB03832ADA65643D0E2,
	GameManager_Idle_m1B814D24018A953231A95CE921D527F5D72568A8,
	GameManager_ChangeStateTo_m0EB6EEFACB0A16F6049687D95B210A230B7A1162,
	GameManager_LookAtCamera_m69EA395CCB0C68DCFAC490A049336DFBB8485B2F,
	GameManager__ctor_mF453CED520617BFB65C52405A964E06CF17DB368,
	GameManager_U3CStartU3Eb__9_0_m29368EA0AB7549755F9B1CDC2414EFE2658A392D,
	GameManager_U3CStartU3Eb__9_1_mF245E1278F28FA57342A322B04BBDAC0827CA898,
	GameManager_U3CStartU3Eb__9_2_m0886715BEAE34D92EFCCF57FBCC819F6A4DE5E3E,
	GameManager_U3CStartU3Eb__9_3_mDDDEA84547A9E737B147D0FB43E1DF821E76C4BC,
	GameManager_U3CStartU3Eb__9_4_m1FA0506EFC2915A80D6C34E447716015910FA92A,
	GameManager_U3CStartU3Eb__9_5_m3FA02DC4D3D232660056CCBDD001B7F5852DF4E6,
	GameManager_U3CStartU3Eb__9_6_m6592677B3AEC7CE85504C0F597A60CF7AB43024B,
	CameraMove_Start_m9BC4AAF6D2DC912359A2DB5C7689DB27DBF4BA51,
	CameraMove_Update_mE9E960AB865A8E65A6E1E57ABA98827B43DBE9BC,
	CameraMove_Rotate_m93DA0C1A259946EB0083EAA54C186913E95A6315,
	CameraMove__ctor_m3BBC2F541B40FB1634F0EA0B844BA27E5DDD9908,
	CollectingHearts_Start_m5F2B6A61085215175C944DAD4EBC37E8850CE31F,
	CollectingHearts_OnTriggerEnter_mA1E6F93DE7ACFEBD7433F4A35BD027E480F20E11,
	CollectingHearts_PauseGame_mAD991EDE285D2A361E2E60425814A14078852CBA,
	CollectingHearts_ResumeGame_m40C2B3C8EE43E4F31DB42E651581C2FAF775E3A0,
	CollectingHearts__ctor_mB8075967156DB1B72C2E0F1940FB5E715F4B5C21,
	DataLoader_Start_m709E7020FCC56E283A6673EA1DEDD2242C8EC3F3,
	DataLoader_Loader_mF42BAE84DD79A156760C9C828504417DE58D74E6,
	DataLoader__ctor_m33F58839236A971EB235C4D9B49E4876A76FB9A6,
	U3CU3Ec__cctor_m417DC2AF817231E3B67113909A95E3648FD91127,
	U3CU3Ec__ctor_mA07A16D73A6BFBBAA099837DDF24F66234E4544F,
	U3CU3Ec_U3CLoaderU3Eb__2_0_m5E88B435BA060F648969A8C06B51509BDBE9B7EE,
	U3CLoaderU3Ed__2__ctor_m1C22865F6551EC4486AB0132E349B86BF442C5BC,
	U3CLoaderU3Ed__2_System_IDisposable_Dispose_mD5A993D64000661634B4E628D38A6AABA3BFE9AD,
	U3CLoaderU3Ed__2_MoveNext_m04292C42B43E416E6F557F6E3834C21C845DB37C,
	U3CLoaderU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFE49CEED3A1A69BB5CB72250C410FF2605E240FE,
	U3CLoaderU3Ed__2_System_Collections_IEnumerator_Reset_m809646CB0C0D8F92E17262F609A069874148F3F8,
	U3CLoaderU3Ed__2_System_Collections_IEnumerator_get_Current_m19FBF2A640655D46061C4B2072EF252BFED6098B,
	funSceneScript_Start_m3795C7F1516764F9E269EFF8558DCF2906D8A6CA,
	funSceneScript_StartGameShape_m20DCD398EBCB4D48F93EF50E0ACECD7B1AB10DC1,
	funSceneScript_StartGameWord_m5D05708C49F5AAE608C2B2782F688767C58D0157,
	funSceneScript_ShowLeaderBoard_m229D4C91E6780AF4B9B0AA45075C6E382AD2A654,
	funSceneScript_GoBack_m1C1211F929D16334D4871458D7A8B60429888B2E,
	funSceneScript_GoToTuto_mD0F1120FAC113933D90838CDE71BFF978B727089,
	funSceneScript_RandomProba_m17E5EEB06E7F241D1E5778493F82567DA8DA378B,
	funSceneScript_RandomColorKey_m3218DDF5720AC64A141BC35ACDBDA7AAE9C612A2,
	funSceneScript_InstantiateQuestion_mDCECC14624D28F88B5DBF9E80E33FF8C44FCB438,
	funSceneScript_Update_mB7C51ACBB4595DB8DFCC4A6188C78A28F519F64A,
	funSceneScript_TimeCountdown_mD7B5B680FB355FCE1544E6ACC6C2F8C3600E72BB,
	funSceneScript_onClickRight_m44CC79043D306386F7F57C89404535E4B6A36D73,
	funSceneScript_onClickLeft_mB5C466B3870D5DA70BA34B221AC573E05920EAEC,
	funSceneScript_Answer_m5E140C2EAFE842C51A32C683D96554396CF8B4A6,
	funSceneScript__ctor_mB373BAF18646E039EFAD19AF1E761F1071536949,
	HomeScript_Start_m46637A2DF00461BAB2FA53661788E7326936B191,
	HomeScript_GetVirtualCurrency_mDBD6E357AE663F715FF9C5A1CFBACBDCA5F9A927,
	HomeScript_Update_m310EB584A5A15E10AE2334F2F5271334A2AFC949,
	HomeScript__ctor_mCDBE87645B9E8E89256F05CD133AADE1B8CFD2AB,
	HomeScript_U3CGetVirtualCurrencyU3Eb__3_0_m21DFCB754583341A95124365E6A0CB258930267C,
	U3CU3Ec__cctor_m6A90C203D144C391CECDF322C2E3B6271D04FE4B,
	U3CU3Ec__ctor_m44983C2F8775B7812CC8F69FF220BA31D0EBDB71,
	U3CU3Ec_U3CGetVirtualCurrencyU3Eb__3_1_mDC0EE5D5E6B3115D56DA5C08E2BE811E8B300139,
	InventoryScript_Start_m4318FC1D1E6FB13FF29E29DDDC0A3053FA12A626,
	InventoryScript_SpawnCardsInInventory_m1A6326ACD9905728220A7BBCCF8C24E555DEE5C5,
	InventoryScript__ctor_m7AF92B24D0AC8339DA8BFACD471A96FCB5A3D26D,
	U3CU3Ec__DisplayClass2_0__ctor_mB9A1E96A96201727DE4A85CD818A84572F49327C,
	U3CU3Ec__DisplayClass2_0_U3CSpawnCardsInInventoryU3Eb__0_m386097060903085E4682C2AD323D6F93191096A2,
	U3CU3Ec__cctor_m9670502EA23C39DC5F131322B7CD456A7BFC11DD,
	U3CU3Ec__ctor_mDB5643258239D638957734AA23AA6C86CA95C84C,
	U3CU3Ec_U3CSpawnCardsInInventoryU3Eb__2_1_mDCEC39AA086E1A52043D1823FFD24EEED66D0099,
	Item_Start_mAF304BC26FB5ED4C69F6534266EE4F082967E4D8,
	Item_BuyItem_m2111DBEEF26EDD456E27CE22F648511F6BAF3FDE,
	Item_onSuccessPurchase_m37750C6CAF6B13C4CE5BE1A9C4C4121B65923040,
	Item_onSuccess_mC5F610693487822D231D0CAB8FE40DB457D385CD,
	Item_onError_m2FB46161DEEA0AAFBD52A24B9CF4AAFED7DA33AA,
	Item__ctor_m741D59B05082743C60D2F1149112B571E89CAFAF,
	LoadingScreenManager_LoadScene_m823C4472BD68911292132F5309BCAF4DC56F16D3,
	LoadingScreenManager_LoadSceneAsync_m49B81CF3E4B7B9704DD289B10CED98416C01D12C,
	LoadingScreenManager__ctor_mE31332B5B0F7414BB3C95D11FDB397E471D75C64,
	U3CLoadSceneAsyncU3Ed__4__ctor_mF8C9188D135C46E469424012BE8399F6784EF3A5,
	U3CLoadSceneAsyncU3Ed__4_System_IDisposable_Dispose_m008D03B3E3B41870AF08FB9D28662D2379DC349F,
	U3CLoadSceneAsyncU3Ed__4_MoveNext_m1495E943131575F35340DD50041A25D98993F775,
	U3CLoadSceneAsyncU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3C9C6775CDE3DFAA853173A02FD10F279B81E331,
	U3CLoadSceneAsyncU3Ed__4_System_Collections_IEnumerator_Reset_mCEFCC4FED8CDD4763CBEAC46D0EC5FB82FF41339,
	U3CLoadSceneAsyncU3Ed__4_System_Collections_IEnumerator_get_Current_m1AA19E6D7A57DC10DB64FCA8139A5A8054724C43,
	PlayerMouv_Start_m1ECD55989605232DC8D390D5DC487F7E5CDE9D95,
	PlayerMouv_Update_m19D4D156D951023F45EAF9F5FE259672CFCD7FB0,
	PlayerMouv_Move_mF55E47F6604D1ED4FA9B184CCF34555771A2F2E3,
	PlayerMouv_Idle_mC96FE4B4FA7B2FCDF8BC948F9D6CF620A25AD277,
	PlayerMouv_Walk_mB1A1EEEDADD306F38B0ABD5BD9F5BFC3C03351B2,
	PlayerMouv_Run_m9D92B6975AA4E5BB7FE224790C90CF4413A58CD3,
	PlayerMouv_Jump_m803E3A3FF4F6E53684DD90820BE0839F5FDF3D35,
	PlayerMouv__ctor_m59281E1A3294DC3E6E6D49F9CBE53A3A4183DA8F,
	PlayFabControls_Start_m7A6CDC8E48983241857C3955BCA8159EE57A1FA4,
	PlayFabControls_Update_m24B2D5174C360A4674DF638261F37910CF1BC04E,
	PlayFabControls_ToSignInPanel_mBACA29C524623D3E627A6E3B048C610FFE77DD2C,
	PlayFabControls_ToSignUpPanel_m26579C369D73A44227F231D5E684C4F245D16D91,
	PlayFabControls_Encrypt_mD046273CBC06052F6BF611740EEE901C702546C5,
	PlayFabControls_SingUp_mAC4145AB251F22D908E6134DB0D921245FE8A70D,
	PlayFabControls_OnRegisterSuccess_mE7A83E89023F6344DDC8082BDFCE6B6487C8FC2D,
	PlayFabControls_OnError_m4A9059F6C1454FB10F0FB57DE44938EF995208EC,
	PlayFabControls_Login_mB1B4CC9AACBF9F642EA958116C088ED2BC7D27C4,
	PlayFabControls_LoginError_m2D4100CE9CC6754AC248FD8A7DA579A3CA3CA077,
	PlayFabControls_LoginSuccess_mE245340D080124FF3C992986DB6FF7F0559BDE4A,
	PlayFabControls_StartGame_m85D7B86D4B84E372C41EB4E4EDD491364014C220,
	PlayFabControls__ctor_m9757752ECF18D8BFCB12C497FF6599182C8B1A00,
	PlayFabLoad_Awake_m27AD8339F852574D6378BDC40EBB1EF88116AB57,
	PlayFabLoad_Start_mE9EF3C2569F4DF0D5E9A6338437C23A552DA881E,
	PlayFabLoad_GetPlayerData_m786D9F5B57DDDA10CA2B92F57666E2BE3C844A9B,
	PlayFabLoad_OnError_m6CFB38002AC2F377FBB7CCFED842F559E122617A,
	PlayFabLoad_OnSuccess_m5792971F46A9659956283BDAA4587EC3321AFA58,
	PlayFabLoad_Update_m823904211C6EE00633D9E81322B34F2545A400DF,
	PlayFabLoad_GetVirtualCurrency_m3691A467586730E966399FCAF96A8361EE25B78E,
	PlayFabLoad_OnGetUserInventorySuccess_mD026BF7A4C3EC7B401C93BCE91DF470E59DD5313,
	PlayFabLoad_Logout_m22F919A55F2FA9547061EAA655CE08B0E1AEAA35,
	PlayFabLoad_GetItems_m7BE33BC810B7EEC47C923169B4784C6E913448C8,
	PlayFabLoad_SpawnItems_mF52B7D2D9589EEF970716E774167CB4BE2B87C5D,
	PlayFabLoad_UpdateDataInScene_m62582E8EDDD8D003A49BB57FE88FDF97929192DA,
	PlayFabLoad_ShapeSendLeaderBoard_m20D75366E160EF2D3F05B5602B934CAD4E3814B7,
	PlayFabLoad_WordSendLeaderBoard_m942C5C13D43B7E7E291230F841D7AA77BA51FC4D,
	PlayFabLoad_OnLeaderBoardUpdate_m08AC5EC0D85182933FB049E1903E90CE204EB743,
	PlayFabLoad_GetLeaderBoardShape_m2281FFBD5A83FD39843A94FFDB9517D653E7CB2F,
	PlayFabLoad_GetLeaderBoardWord_m9A3BCC9522AB2C3A8A249F38C1A646A56546083F,
	PlayFabLoad_OnLeaderBoardGetShape_m7CC311B539EB13AA8022DA3C063D35A81ABB34E0,
	PlayFabLoad_OnLeaderBoardGetWord_m144B524D91FA15AA4B6310919B4FA75FFE2BDE51,
	PlayFabLoad__ctor_m645447EB5F8FB3FF9E6629599C9919C22FDA5CD1,
	rotateCamera_Start_m93E06F1A9FEF74BAF2A47C14E7584828CE13760C,
	rotateCamera_Update_m1A67811A414A7B9A39E66B747FB4512A8270B86B,
	rotateCamera__ctor_m6996E5AFAB42DA2CAD20BBD5A0D89D9D0A989848,
	StoreScript_Start_m5114A80C6D071937AFE843C7AB27F811FD77FF73,
	StoreScript_SpawnCardsInStore_m46ABE21977A9C55C05C31212DA20DB44C951A8F1,
	StoreScript__ctor_m83569B6502B9B8B35A6CFDB51ABC815E875A9E30,
	U3CU3Ec__DisplayClass2_0__ctor_m8673C710F9DFA85E5357E9C443779549A9C6A135,
	U3CU3Ec__DisplayClass2_0_U3CSpawnCardsInStoreU3Eb__0_m7C50F758398361106429A37EE363077E0CDEEC3C,
	U3CU3Ec__cctor_m21AFC931D1509D7EF8A21AE21F83F124B9EBF3FD,
	U3CU3Ec__ctor_m61B4D48A22F0EAF55FDF885CAA55B0A1BD315683,
	U3CU3Ec_U3CSpawnCardsInStoreU3Eb__2_1_m552AE3DF7606B29A07A203C5B25B644EF1FE349A,
	testSceneScript_Start_mC3A592D5A1873C81BC22C160745BC445EA6ACCAC,
	testSceneScript_RandomProba_m16A5807F01FE9475B5114E0A08B510E46DE63729,
	testSceneScript_RandomColorKey_mE807223976F5FBCCF1FB7576078FED68290949FB,
	testSceneScript_StartGameShape_m4B1EA5E1632F85EF5307F1ED3173E4F17E08B1B5,
	testSceneScript_StartGameWord_m8C7C9BD67F112D609717965843151A2EAF081079,
	testSceneScript_InstantiateQuestion_m7013473D9A3AB0C4958490FC317F30B0F0CDC603,
	testSceneScript_Update_m7CC05B6DCAA0BA93FF095C5FBC3AFB2B3E1E1487,
	testSceneScript_TimeCountdown_m2C9FA57BE7B6DE9C171405809D12E531EF8A92E9,
	testSceneScript_onClickRight_m2BC016EF691C335B788990A06A666E3C345C8415,
	testSceneScript_onClickLeft_mF21360C0F118BE7BAC986A29C26879A0DDCEEDDE,
	testSceneScript_Answer_m345AF5F3E370F02FA2078024BE625CB3390E336B,
	testSceneScript__ctor_m687CD5473FECC273DCABB249022534CEBDE163D5,
	AbstractTargetFollower_Start_m900ACFC81D342429D83E274C92B69467E443763D,
	AbstractTargetFollower_FixedUpdate_mB21B34361FB6322B028B8087893942F3E9A11A22,
	AbstractTargetFollower_LateUpdate_m15329BC030A27BD5C92BCF0B9A2E7DC27C4ED202,
	AbstractTargetFollower_ManualUpdate_mC0EDA156049123462E7E0507191183A112209C58,
	NULL,
	AbstractTargetFollower_FindAndTargetPlayer_mEBAED59C944A601F015CE6390C110B00EA882F2D,
	AbstractTargetFollower_SetTarget_m28BD1FECB5D3F99925BCA617888401258BD8A28C,
	AbstractTargetFollower_get_Target_m0A358E204FE5247E0702B24B3D2047E89815A3D0,
	AbstractTargetFollower__ctor_m7BA92F6CC37CDA403608738E4F6CB8EB43889A98,
	FreeLookCam_Awake_m6DF1C77903C277F5D7111A267F9B71B708A50867,
	FreeLookCam_Update_m1158A615C803765D6375B456ADB1DDF93D8E7FB8,
	FreeLookCam_OnDisable_mCD044A476BAF96C34A06162121D3D21664AD438E,
	FreeLookCam_FollowTarget_m807D7425DA3AC99BCFC995FC3F53B3139F50FE7E,
	FreeLookCam_HandleRotationMovement_m0E9A0819AA068EFA603FB0CC3318FBE71827A97F,
	FreeLookCam__ctor_m0B5161AC16134D026A74E23EDE3011F252B9C28B,
	PivotBasedCameraRig_Awake_m3477C8EE2F83D51CCF81ADC1F8BEEDCB0A2F95B2,
	PivotBasedCameraRig__ctor_m176687FB94C7FF07D4ED8B38A0529F370F9D51BA,
};
static const int32_t s_InvokerIndices[173] = 
{
	4991,
	4991,
	4991,
	4991,
	4991,
	4991,
	4041,
	4991,
	4991,
	4041,
	4991,
	4991,
	4991,
	4991,
	4991,
	4022,
	4991,
	4991,
	4991,
	4991,
	4991,
	4991,
	4991,
	4991,
	4991,
	4991,
	4991,
	4991,
	4991,
	4991,
	4041,
	4991,
	4991,
	4991,
	4991,
	4880,
	4991,
	7404,
	4991,
	4808,
	4022,
	4991,
	4808,
	4880,
	4991,
	4880,
	4991,
	4991,
	4991,
	4991,
	4991,
	4991,
	4858,
	4858,
	4991,
	4991,
	4991,
	4991,
	4991,
	3969,
	4991,
	4991,
	4991,
	4991,
	4991,
	4041,
	7404,
	4991,
	4041,
	4991,
	4041,
	4991,
	4991,
	4041,
	7404,
	4991,
	4041,
	4991,
	4991,
	4041,
	4041,
	4041,
	4991,
	4022,
	3587,
	4991,
	4022,
	4991,
	4808,
	4880,
	4991,
	4880,
	4991,
	4991,
	4991,
	4991,
	4991,
	4991,
	4991,
	4991,
	4991,
	4991,
	4991,
	4991,
	3590,
	4991,
	4041,
	4041,
	4991,
	4041,
	4041,
	4991,
	4991,
	4991,
	4991,
	4991,
	4041,
	4041,
	4991,
	4991,
	4041,
	4991,
	4991,
	4991,
	4991,
	4022,
	4022,
	4041,
	4991,
	4991,
	4041,
	4041,
	4991,
	4991,
	4991,
	4991,
	4991,
	4041,
	4991,
	4991,
	4041,
	7404,
	4991,
	4041,
	4991,
	4858,
	4858,
	4991,
	4991,
	4991,
	4991,
	4991,
	4991,
	4991,
	3969,
	4991,
	4991,
	4991,
	4991,
	4991,
	0,
	4991,
	4041,
	4880,
	4991,
	4991,
	4991,
	4991,
	4086,
	4991,
	4991,
	4991,
	4991,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	173,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
